﻿using Loggers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace DataSaver {

    [Serializable]
    class SaveInfo<T> where T : class {
        public T Data { get; set; }
    }

    public abstract class Store {
        public abstract Type Type { get; }

        public static Store CreateInstance(Type type) {
            Type genericClass = typeof(Store<>);
            Type constructedClass = genericClass.MakeGenericType(type);
            object instance = Activator.CreateInstance(constructedClass);

            return (Store)instance;
        }

        public abstract bool Save();
        public abstract bool Save(object obj);

        public abstract object Load();
    }

    public class Store<T> : Store where T : class {
        readonly string _dataFilename;
        readonly ILogger _logger;

        public override Type Type => typeof(T);

        private SaveInfo<T> saveInfo;

        public Store(string filename, ILogger logger) {
            _dataFilename = filename;
            _logger = logger;
            saveInfo = new SaveInfo<T>();
        }

        public Store(string filename) : this(filename, new EmptyLogger()){
            _dataFilename = filename;
        }

        public Store() : this(typeof(T).ToString(), new EmptyLogger()) { }

        public override bool Save(object obj) {
            saveInfo.Data = (T)obj;

            try {
                using (FileStream fileStream = new FileStream(_dataFilename, FileMode.Create, FileAccess.Write)) {
                    new BinaryFormatter().Serialize(fileStream, saveInfo);
                }
                _logger.Write("Saved data to {0}\nDetails: {1}", _dataFilename, saveInfo.Data.ToString());
                return true;
            } catch (Exception e) {
                _logger.Write(e);
                return false;
            }
        }

        public override object Load() {
            if (File.Exists(_dataFilename)) {
                try {
                    using (FileStream fileStream = new FileStream(_dataFilename, FileMode.Open, FileAccess.Read)) {
                        fileStream.Position = 0;
                        saveInfo = (SaveInfo<T>)(new BinaryFormatter()).Deserialize(fileStream);
                    }
                    // TODO: log details
                    _logger.Write("Successfully loaded data from {0}\nDetails: {1}", _dataFilename, "");
                    return saveInfo.Data;
                } catch (Exception e) {
                    _logger.Write(e);
                }
            }
            return default(T);
        }
    }
}
