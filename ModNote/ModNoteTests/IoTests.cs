﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModNote.ModNote;
using DataSaver;

namespace ModNoteTests {
    [TestClass]
    public class IoTests {
        [TestMethod]
        public void ModuleCanBesSaved() {
            var module = new Module {
                Code = "123",
                Title = "Memes"
            };
            var store = Store.CreateInstance(typeof(Module));
            store.Save(module);

            var data = store.Load();

            var moduleRoundTwo = (Module)data;

            Assert.AreEqual(module.Code, moduleRoundTwo.Code);
        }
    }
}
