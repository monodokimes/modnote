﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModNote.ModNote {
    [Serializable]
    public class Module {
        public string Code { get; set; }
        public string Title { get; set; }
        public string Synopsis { get; set; }
        public List<LearningObjective> LearningObjectives { get; set; }
        public List<Assignment> Assignments { get; set; }
        public List<Note> Notes { get; set; }
    }
}
