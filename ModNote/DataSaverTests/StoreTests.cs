﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataSaver;
using System.IO;

namespace DataSaverTests {
    [Serializable]
    public class SerializableTestClass {
        public int Number { get; set; }
        public string String { get; set; }
    }

    public class NonSerializableTestClass {
        public int Number { get; set; }
        public string String { get; set; }
    }

    [TestClass]
    public class StoreTests {
        private string filename = "modnotesavedata.dat";
        private SerializableTestClass serializableTestClass = new SerializableTestClass {
            Number = 1,
            String = "A"
        };
        private NonSerializableTestClass nonSerializableTestClass = new NonSerializableTestClass {
            Number = 1,
            String = "A"
        };

        private Type testType = typeof(Type);

        private void ClearFile() {
            if (File.Exists(filename))
                File.Delete(filename);
        }
        
        [TestMethod]
        public void Save_SerializableTestClass_CreatesFile() {
            ClearFile();
            var store = new Store<SerializableTestClass>(filename);
            var testClass = serializableTestClass;

            store.Save(testClass);

            Assert.IsTrue(File.Exists(filename));
        }

        [TestMethod]
        public void Save_NonSerializableTestClass_CreatesFile() {
            ClearFile();
            var store = new Store<NonSerializableTestClass>(filename);
            var testClass = nonSerializableTestClass;

            store.Save(testClass);

            Assert.IsTrue(File.Exists(filename));
        }

        [TestMethod]
        public void Load_SerializableTestClass_DataIsCorrect() {
            ClearFile();
            var store = new Store<SerializableTestClass>();
            var testClass = serializableTestClass;

            store.Save(testClass);
            var data = (SerializableTestClass)store.Load();

            Assert.IsNotNull(data);
            Assert.AreEqual(data.Number, testClass.Number);
            Assert.AreEqual(data.String, testClass.String);
        }

        [TestMethod]
        public void Load_NonSerializableTestClass_DataIsNull() {
            ClearFile();
            var store = new Store<NonSerializableTestClass>();
            var testClass = nonSerializableTestClass;

            store.Save(testClass);
            var data = store.Load();

            Assert.IsNull(data);
        }

        [TestMethod]
        public void Type_NonSerializableClass_ReturnsCorrectType() {
            var store = new Store<NonSerializableTestClass>();

            Assert.AreEqual(store.Type, typeof(NonSerializableTestClass));
        }

        [TestMethod]
        public void Type_SerializableClass_ReturnsCorrectType() {
            var store = new Store<SerializableTestClass>();

            Assert.AreEqual(store.Type, typeof(SerializableTestClass));
        }

        [TestMethod]
        public void CreateInstance_SerializableTestClass_CorrectObject() {
            Type type = typeof(SerializableTestClass);

            Store created = Store.CreateInstance(type);

            Assert.IsNotNull(created.Type);
        }
    }
}
