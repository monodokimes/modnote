﻿using DataSaver;
using ModNote.ModNote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModNote {
    public class DataManager {
        List<Type> _types = new List<Type> {
            typeof(Module),
            typeof(Assignment),
            typeof(Note)
        };

        List<Store> _stores;

        public DataManager() {
            _stores = new List<Store>();

            _types.ForEach(t => _stores.Add(Store.CreateInstance(t)));
        }

        public DataManager(IEnumerable<Type> types) {

        }
    }
}
