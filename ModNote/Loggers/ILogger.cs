﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loggers {
    public interface ILogger {
        void Write(string text);
        void Write(string pattern, params string[] args);
        void WriteException(Exception exception);
    }
}
