﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loggers {
    public abstract class Logger : ILogger {
        public virtual void WriteException(Exception exception) {
            Write(exception.ToString());
        }

        public virtual void Write(string text) {
            text = string.Format("[{0}] - {1}", DateTime.Now.TimeOfDay.ToString(), text);
        }

        public void Write(string pattern, params string[] args) {
            Write(string.Format(pattern, args));
        }
    }
}
