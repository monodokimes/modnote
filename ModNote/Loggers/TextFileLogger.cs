﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loggers {
    public class TextFileLogger : Logger {

        readonly string _logFilename;
        
        public TextFileLogger(string filename) {
            _logFilename = filename;
        }

        // TODO: This will introduce a bug unless singleton pattern is implemented
        //
        // Multiple instances of TextFileLogger will be instantiated at different times
        // resulting in several log files for a single run of the program
        public TextFileLogger() : this("run_" + DateTime.Now.ToString()) {

        }

        public override void Write(string text) {
            base.Write(text);

            if (!File.Exists(_logFilename)) 
                File.Create(_logFilename);

            using (StreamWriter streamWriter = new StreamWriter(_logFilename)) 
                streamWriter.WriteLine(text);
        }


    }
}
